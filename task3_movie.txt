1. What was the point made by Bill Gates in his Open Letter to Hobbyists?
The point was that people were using his company's software without having to pay for it 
because it was being shared around. This discouraged him and other developers on making 
good software because it was unfair that people were able to get a hold of the software 
that they made for free while they invested a lot of their time, effort and money on 
creating it. 

2. What did Richard Stallman describe as an ethical dilemma?
Modern computers back in the day needed proprietary operating systems and the developers
of those systems forced people to sign a contract preventing them to share the product. 
This hindered their progression because it was a very cooperative community but
the fact that they weren't allowed to share the product slowed them down. So the choices
were either you get the newest computer and only help yourself or not to get it and prevent
yourself from doing useful things.

3. What are the freedoms that enable people to form a community?
Encourage everyone to share a free operating system which also keeps people away from using 
computers without betraying other people. This would also give people a way out of the 
ethical dilemma.
